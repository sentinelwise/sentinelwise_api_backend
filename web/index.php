<?php

require_once __DIR__ . '/../vendor/autoload.php';

function pre() {
    foreach (func_get_args() as $arg) {
        echo '<pre>' . print_r($arg, true) . '</pre>';
    }
}

$app = new Silex\Application();
$app['debug'] = true;

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver' => 'pdo_mysql',
        'host' => 'localhost',
        'dbname' => 'buildar',
        'user' => 'root',
        'password' => '',
    ),
));

$app->get('/deformation/{area}', function(Silex\Application $app, $area) {
    //$test = $app['db']->fetchAssoc('SELECT * FROM pixels');
    return $app->json(array(
                'area' => $area,
                'deformationscore' => 5,
                'timeline' => array(
                    '2015-06-01 00:00:00' => 1,
                    '2015-06-02 00:00:00' => 2,
                    '2015-06-03 00:00:00' => 3,
                    '2015-06-04 00:00:00' => 4,
                    '2015-06-05 00:00:00' => 5,
                    '2015-06-06 00:00:00' => 3,
                    '2015-06-07 00:00:00' => 2,
                ),
    ));
});

$app->get('/address/{address}', function(Silex\Application $app, $address) {
    return $app->json(array(
                'address' => $address,
                'deformationscore' => 5,
                'timeline' => array(
                    '2015-06-01 00:00:00' => 1,
                    '2015-06-02 00:00:00' => 2,
                    '2015-06-03 00:00:00' => 3,
                    '2015-06-04 00:00:00' => 4,
                    '2015-06-05 00:00:00' => 5,
                    '2015-06-06 00:00:00' => 3,
                    '2015-06-07 00:00:00' => 2,
                ),
    ));
});

$app->run();
